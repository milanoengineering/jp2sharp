﻿using System.IO;

namespace JP2Sharp.Util
{
	public interface IFileStreamCreator
	{
		Stream Create(string path, string mode);
	}
}