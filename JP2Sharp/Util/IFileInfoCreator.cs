﻿namespace JP2Sharp.Util
{
	public interface IFileInfoCreator
	{
		#region METHODS

		IFileInfo Create(string fileName);

		#endregion
	}
}