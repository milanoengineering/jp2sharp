﻿namespace JP2Sharp.Util
{
	internal class WinformsBitmapWrapperCreator : IBitmapWrapperCreator
	{
		#region METHODS

		public IBitmapWrapper Create(int width, int height, int numberOfComponents)
		{
			return new WinformsBitmapWrapper(width, height, numberOfComponents);
		}
		
		#endregion
	}
}