﻿namespace JP2Sharp.Util
{
	public interface IBitmapWrapperCreator
	{
		IBitmapWrapper Create(int width, int height, int numberOfComponents);
	}
}