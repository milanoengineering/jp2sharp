﻿using System;
using JP2Sharp.j2k.util;

namespace JP2Sharp.Util
{
#if MEF
	[System.ComponentModel.Composition.Export(typeof(IMsgLogger))]
#endif
	public class DotnetMsgLogger : IMsgLogger
	{
#if MEF
		#region CONSTRUCTORS

		public DotnetMsgLogger()
		{
			Register();
		}

		#endregion
#endif

		#region METHODS

		public static void Register()
		{
			FacilityManager.DefaultMsgLogger = new StreamMsgLogger(Console.OpenStandardOutput(), Console.OpenStandardError(), 78);
		}

		#endregion
	}
}