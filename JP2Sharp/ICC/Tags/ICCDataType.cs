using System;
using JP2Sharp.ICC;

namespace JP2Sharp.ICC.Tags
{
    public class ICCDataType : ICCTag
    {
        new public int type;
        public int reserved;
        public int dataFlag;
        //byte[] Data;

        /// <summary> Construct this tag from its constituant parts</summary>
        /// <param name="signature">tag id</param>
        /// <param name="data">array of bytes</param>
        /// <param name="offset">to data in the data array</param>
        /// <param name="length">of data in the data array</param>
        protected internal ICCDataType(int signature, byte[] data, int offset, int length)
            : base(signature, data, offset, offset + 2 * ICCProfile.IntSize)
        {
            type = ICCProfile.getInt(data, offset);
            reserved = ICCProfile.getInt(data, offset + ICCProfile.IntSize);
            dataFlag = ICCProfile.getInt(data, offset + ICCProfile.IntSize);
            //Data = ICCProfile.getString(data, offset + ICCProfile.int_size, length, true);
        }
    }
}