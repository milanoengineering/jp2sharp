/// <summary>**************************************************************************
/// 
/// $Id: LookUpTable16Interp.java,v 1.1 2002/07/25 14:56:46 grosbois Exp $
/// 
/// Copyright Eastman Kodak Company, 343 State Street, Rochester, NY 14650
/// $Date $
/// ***************************************************************************
/// </summary>
using System;
using JP2Sharp.ICC.Tags;

namespace JP2Sharp.ICC.LUT
{
	
	/// <summary> An interpolated 16 bit lut
	/// 
	/// </summary>
	/// <version> 	1.0
	/// </version>
	/// <author> 	Bruce A.Kern
	/// </author>
	public class LookUpTable16Interp:LookUpTable16
	{
		
		/// <summary> Construct the lut from the curve data</summary>
		/// <oaram>   curve the data </oaram>
		/// <oaram>   dwNumInput the lut size </oaram>
		/// <oaram>   dwMaxOutput the lut max value </oaram>
		public LookUpTable16Interp(ICCCurveType curve, int dwNumInput, int dwMaxOutput) : base(curve, dwNumInput, dwMaxOutput)
		{
			// Indices of interpolation points
			int dwLowIndex, dwHighIndex;

			// FP indices of interpolation points
			double dfLowIndex, dfHighIndex;

			// Target index into interpolation table
			double dfTargetIndex;

			// Ratio of LUT input points to curve values
			double dfRatio;

			// Interpolation values
			double dfLow, dfHigh;

			// Output LUT value
			double dfOut;
			
			dfRatio = (curve.count - 1) / (double)(dwNumInput - 1);
			
			for (int i = 0; i < dwNumInput; i++)
			{
				dfTargetIndex = i * dfRatio;
				dfLowIndex = Math.Floor(dfTargetIndex);

				dwLowIndex = (int) dfLowIndex;
				dfHighIndex = Math.Ceiling(dfTargetIndex);
				dwHighIndex = (int) dfHighIndex;

				if (dwLowIndex == dwHighIndex)
				{
					dfOut = ICCCurveType.CurveToDouble(curve.entry(dwLowIndex));
				}
				else
				{
					dfLow = ICCCurveType.CurveToDouble(curve.entry(dwLowIndex));
					dfHigh = ICCCurveType.CurveToDouble(curve.entry(dwHighIndex));
					dfOut = dfLow + (dfHigh - dfLow)*(dfTargetIndex - dfLowIndex);
				}

				lut[i] = (short) Math.Floor(dfOut * dwMaxOutput + 0.5);
			}
		}
	}
}