/// <summary>**************************************************************************
/// 
/// $Id: LookUpTable16Gamma.java,v 1.1 2002/07/25 14:56:46 grosbois Exp $
/// 
/// Copyright Eastman Kodak Company, 343 State Street, Rochester, NY 14650
/// $Date $
/// ***************************************************************************
/// </summary>
using System;
using JP2Sharp.ICC.Tags;

namespace JP2Sharp.ICC.LUT
{
	
	/// <summary> A Gamma based 16 bit lut.
	/// 
	/// </summary>
	/// <seealso cref="ICCCurveType">
	/// </seealso>
	/// <version> 	1.0
	/// </version>
	/// <author> 	Bruce A. Kern
	/// </author>
	public class LookUpTable16Gamma : LookUpTable16
	{
		/// <summary>
		/// Construct the LUT
		/// </summary>
		/// <param name="curve">curve data</param>
		/// <param name="dwNumInput">size of lut</param>
		/// <param name="dwMaxOutput">max value of lut</param>
		public LookUpTable16Gamma(ICCCurveType curve, int dwNumInput, int dwMaxOutput) : base(curve, dwNumInput, dwMaxOutput)
		{
			// Gamma exponent for inverse transformation
			double dfE = ICCCurveType.CurveGammaToDouble(curve.entry(0));

			for (int i = 0; i < dwNumInput; i++)
			{
				lut[i] = (short) System.Math.Floor(System.Math.Pow((double) i / (dwNumInput - 1), dfE) * dwMaxOutput + 0.5);
			}
		}
	}
}