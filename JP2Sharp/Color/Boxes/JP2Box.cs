/// <summary>**************************************************************************
/// 
/// $Id: JP2Box.java,v 1.1 2002/07/25 14:50:47 grosbois Exp $
/// 
/// Copyright Eastman Kodak Company, 343 State Street, Rochester, NY 14650
/// $Date $
/// ***************************************************************************
/// </summary>
using System;
using System.Collections.Generic;
using System.IO;
using JP2Sharp.ICC;
using JP2Sharp.JP2.fileformat;

namespace JP2Sharp.Color.Boxes
{
	
	/// <summary> The abstract super class modeling the aspects of
	/// a JP2 box common to all such boxes.
	/// 
	/// </summary>
	/// <version> 	1.0
	/// </version>
	/// <author> 	Bruce A. Kern
	/// </author>
	public abstract class JP2Box
	{
		/// <summary>Platform dependant line terminator </summary>
		public static readonly String Eol = Environment.NewLine;

		/// <summary>Box type</summary>
		public static int Type;
		
		/// <summary>Return a String representation of the Box type. </summary>
		public static string getTypeString(int t)
		{
			return BoxType.get_Renamed(t);
		}
		
		/// <summary>Length of the box.</summary>
		public int Length;

		/// <summary>input file </summary>
		protected internal JP2.io.RandomAccessIO input;

		/// <summary>offset to start of box</summary>
		protected internal int boxStart;

		/// <summary>offset to end of box</summary>
		protected internal int boxEnd;

		/// <summary>offset to start of data in box</summary>
		protected internal int dataStart;
		
		/// <summary> Construct a JP2Box from an input image.</summary>
		/// <param name="in">RandomAccessIO jp2 image
		/// </param>
		/// <param name="boxStart">offset to the start of the box in the image
		/// </param>
		/// <exception cref="IOException">ColorSpaceException 
		/// </exception>
		public JP2Box(JP2.io.RandomAccessIO input, int boxStart)
		{
			byte[] boxHeader = new byte[16];
			
			this.input = input;
			this.boxStart = boxStart;
			
			this.input.seek(this.boxStart);
			this.input.readFully(boxHeader, 0, 8);
			
			this.dataStart = boxStart + 8;
            this.Length = ICCProfile.getInt(boxHeader, 0);
			this.boxEnd = boxStart + Length;
			if (Length == 1)
				throw new ColorSpaceException("extended length boxes not supported");
		}
		
		
		/// <summary>Return the box type as a String. </summary>
		public virtual string getTypeString()
		{
			return BoxType.get_Renamed(JP2Box.Type);
		}
		
		
		/// <summary>JP2 Box structure analysis help </summary>
		protected internal class BoxType
		{
			private static Dictionary<int, string> map = new Dictionary<int, string>();
			
			private static void Put(int type, string desc)
			{
				map[type] = desc;
			}
			
			public static string get_Renamed(int type)
			{
				return map[type];
			}
			
			/* end class BoxType */
			static BoxType()
			{
				Put(FileFormatBoxes.BITS_PER_COMPONENT_BOX, "BITS_PER_COMPONENT_BOX");
				Put(FileFormatBoxes.CAPTURE_RESOLUTION_BOX, "CAPTURE_RESOLUTION_BOX");
				Put(FileFormatBoxes.CHANNEL_DEFINITION_BOX, "CHANNEL_DEFINITION_BOX");
				Put(FileFormatBoxes.COLOUR_SPECIFICATION_BOX, "COLOUR_SPECIFICATION_BOX");
				Put(FileFormatBoxes.COMPONENT_MAPPING_BOX, "COMPONENT_MAPPING_BOX");
				Put(FileFormatBoxes.CONTIGUOUS_CODESTREAM_BOX, "CONTIGUOUS_CODESTREAM_BOX");
				Put(FileFormatBoxes.DEFAULT_DISPLAY_RESOLUTION_BOX, "DEFAULT_DISPLAY_RESOLUTION_BOX");
				Put(FileFormatBoxes.FILE_TYPE_BOX, "FILE_TYPE_BOX");
				Put(FileFormatBoxes.IMAGE_HEADER_BOX, "IMAGE_HEADER_BOX");
				Put(FileFormatBoxes.INTELLECTUAL_PROPERTY_BOX, "INTELLECTUAL_PROPERTY_BOX");
				Put(FileFormatBoxes.JP2_HEADER_BOX, "JP2_HEADER_BOX");
				Put(FileFormatBoxes.JP2_SIGNATURE_BOX, "JP2_SIGNATURE_BOX");
				Put(FileFormatBoxes.PALETTE_BOX, "PALETTE_BOX");
				Put(FileFormatBoxes.RESOLUTION_BOX, "RESOLUTION_BOX");
				Put(FileFormatBoxes.URL_BOX, "URL_BOX");
				Put(FileFormatBoxes.UUID_BOX, "UUID_BOX");
				Put(FileFormatBoxes.UUID_INFO_BOX, "UUID_INFO_BOX");
				Put(FileFormatBoxes.UUID_LIST_BOX, "UUID_LIST_BOX");
				Put(FileFormatBoxes.XML_BOX, "XML_BOX");
			}
		}

	}
}